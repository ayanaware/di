/**
 * Indicates how a dependency is injected inside a record.
 */
export enum InjectType {
	/**
	 * The dependency was not injected by mechanisms of the {@link Injector} itself.
	 */
	UNKNOWN = -1,
	/**
	 * The dependency was injected via constructor.
	 */
	CONSTRUCTOR = 0,
	/**
	 * The dependency was injected via property.
	 */
	PROPERTY = 1,
}
