import { DependencyRecord } from './DependencyRecord';

/**
 * Primary data structure for all managed classes, factories and values of a {@link Injector}.
 * Each record also always has a {@link Token} which represents the key.
 */
export interface Record {
	/**
	 * A function to be called when instantiating this record.
	 *
	 * This will be only set for {@link TypeProvider}s, {@link ClassProvider}s and {@link FactoryProvider}s.
	 */
	fn: Function;
	/**
	 * An array of dependency records this record needs before it can be instantiated or used.
	 */
	deps: Array<DependencyRecord>;
	/**
	 * Whether to call {@link Record.fn} as a function or using the *new* keyword.
	 */
	useNew: boolean;
	/**
	 * The value of this record.
	 *
	 * This will only be set if {@link Record.multi} is *false* and after instantiation for {@link TypeProvider}s, {@link ClassProvider}s and {@link FactoryProvider}s.
	 * An {@link ExistingProvider} will have the {@link Token} of the existing target set.
	 * A value provider will always have this set.
	 *
	 * Note that *null* and *undefined* are considered valid values.
	 */
	value: any;
	/**
	 * Whether this record is a multi record or not.
	 *
	 * When set to *true*, this declares the record as a multi-injectable, meaning every request for a value of this record will call {@link Record.fn} and return its value.
	 *
	 * This can only be *true* for {@link TypeProvider}s, {@link ClassProvider}s and {@link FactoryProvider}s.
	 */
	multi: boolean;
	/**
	 * Whether this records value references an existing provider or not.
	 *
	 * This can only be *true* for {@link ExistingProvider}s.
	 */
	existing: boolean;
	/**
	 * Whether this record is lazily loaded or not.
	 *
	 * When set to *true*, this record will only be instantiated if one other injectable requires it.
	 *
	 * This can only be *true* for {@link TypeProvider}s, {@link ClassProvider}s and {@link FactoryProvider}s.
	 */
	lazy: boolean;
}
