import { InjectType } from './InjectType';
import { Token } from './Token';

/**
 * A data structure for dependencies of a {@link Record}.
 */
export interface DependencyRecord {
	/**
	 * The token of the dependency.
	 */
	token: Token;
	/**
	 * The way this injection was done.
	 */
	injectType: InjectType;
	/**
	 * The property key of the injection.
	 * This is only set when {@link InjectMetadata.injectType} is set to {@link InjectType.PROPERTY}.
	 */
	propertyKey?: string | symbol;
	/**
	 * The parameter index of the injection.
	 * This is only set when {@link InjectMetadata.injectType} is set to {@link InjectType.CONSTRUCTOR}.
	 */
	parameterIndex?: number;
}
