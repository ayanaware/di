import { ForwardRef } from '../forwardRef';

import { Token } from './Token';
import { Type } from './Type';

/**
 * Provides a value for a specified {@link Token}.
 */
export interface ValueProvider {
	/**
	 * The token to provide the value under.
	 */
	provide: Token;
	/**
	 * The value itself.
	 */
	useValue: any;
}

/**
 * Provides a class under a specified {@link Token}.
 *
 * If the token and the class are identical, use a {@link TypeProvider} instead.
 */
export interface ClassProvider {
	/**
	 * The token to provide the class under.
	 */
	provide: Token;
	/**
	 * The class itself, or a {@link forwardRef} to it.
	 */
	useClass: Type<any> | ForwardRef<Type<any>>;
}

/**
 * Provides an instance of a class under a specified {@link Token}.
 *
 * If the instances class has any property injections declared, they will be resolved as dependencies.
 */
export interface InstanceProvider {
	/**
	 * The token to provide the instance under.
	 */
	provide: Token;
	/**
	 * The instance itself.
	 */
	useInstance: any;
}

/**
 * Provides a factory under a specified {@link Token}
 */
export interface FactoryProvider {
	/**
	 * The token to provide the factory under.
	 */
	provide: Token;
	/**
	 * The factory function itself.
	 */
	useFactory: (...args: Array<any>) => any;
	/**
	 * Whether to handle this factory as a multi-injectable or not.
	 *
	 * @see {@link Record.multi}
	 */
	multi?: boolean;
	/**
	 * Whether to handle this factory as a lazy-dependency or not.
	 * Setting this to *true* for a multi-injectable will have no effect, as all multi-injectables are lazily instantiated.
	 *
	 * @see {@link Record.lazy}
	 */
	lazy?: boolean;
	/**
	 * The dependencies of this factory passed to the funciton in the same order as given in this array.
	 */
	deps?: Array<Token>;
}

/**
 * Provides an existing {@link Token} under the specified {@link token}.
 */
export interface ExistingProvider {
	/**
	 * The token to provide the provider of the other token under.
	 */
	provide: Token;
	/**
	 * The token that is used to find the actual provider.
	 */
	useExisting: Token;
}

/**
 * Provides a {@link Type} using the type itself as a {@link Token}.
 */
export interface TypeProvider extends Type<any> {}

/**
 * Type alias for all providers.
 */
export type Provider = ValueProvider | ClassProvider | InstanceProvider | FactoryProvider | ExistingProvider | TypeProvider;
