import '@ayana/test';

import 'reflect-metadata';

import { Injectable } from './Injectable';
import { MetadataSymbol } from './MetadataSymbol';

autoDescribe(() => {
	it('should declare proper default injectable metadata on the class', function() {
		@Injectable() class A { }

		expect(
			Reflect.getMetadata(MetadataSymbol.INJECTABLE, A).multi,
			'to be false'
		);
	});

	it('should use the given multi boolean when declaring the metadata', function() {
		@Injectable({ multi: true, lazy: true }) class A { }

		expect(
			Reflect.getMetadata(MetadataSymbol.INJECTABLE, A).multi,
			'to be true'
		);

		expect(
			Reflect.getMetadata(MetadataSymbol.INJECTABLE, A).lazy,
			'to be true'
		);
	});
});
