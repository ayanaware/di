import { IllegalAccessError } from '@ayana/errors';

import { ForwardRef } from '../forwardRef';
import { InjectType, Token } from '../types';

import { InjectMetadata } from './InjectMetadata';
import { MetadataSymbol } from './MetadataSymbol';

/**
 * Decorator to declare an injection on a non-static class property or constructor argument.
 *
 * When dealing with circular dependencies it is recommended to always use a {@link forwardRef} between the injectables that circulary reference each other.
 *
 * Constructor-Injections can also be inferred when the {@link https://www.typescriptlang.org/docs/handbook/decorators.html|emitDecoratorMetadata} option is enabled.
 *
 * @param token The token or a {@link forwardRef} for a token.
 *
 * @returns *PropertyDecorator* & *ParameterDecorator*
 */
export function Inject(token?: Token | ForwardRef<any>): PropertyDecorator & ParameterDecorator {
	return function(target: any, propertyKey?: string | symbol, parameterIndex?: number) {
		let metadataTarget = target;
		if (metadataTarget.prototype === undefined) {
			metadataTarget = metadataTarget.constructor;
		}

		if (typeof parameterIndex === 'number' && propertyKey != null) {
			throw new IllegalAccessError('Cannot use the Inject decorator on method parameters outside of the constructor');
		}
		if (target !== metadataTarget.prototype && propertyKey != null) {
			throw new IllegalAccessError('Cannot use the Inject decorator on static class properties');
		}

		const injectMetadata: Array<InjectMetadata> = Reflect.getMetadata(MetadataSymbol.INJECTIONS, metadataTarget) || [];

		injectMetadata.push({
			token,
			injectType: typeof parameterIndex === 'number' ? InjectType.CONSTRUCTOR : InjectType.PROPERTY,
			propertyKey,
			parameterIndex,
		});

		Reflect.defineMetadata(MetadataSymbol.INJECTIONS, injectMetadata, metadataTarget);
	};
}
