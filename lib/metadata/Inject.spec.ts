import '@ayana/test';

import 'reflect-metadata';

import { InjectType } from '../types/InjectType';

import { Inject } from './Inject';
import { MetadataSymbol } from './MetadataSymbol';

autoDescribe(() => {
	it('should declare metadata for constructor parameters', function() {
		class A { public constructor(@Inject('Token') public value: any) {} }

		expect(
			Reflect.getMetadata(MetadataSymbol.INJECTIONS, A),
			'to contain',
			{ token: 'Token', parameterIndex: 0, injectType: InjectType.CONSTRUCTOR }
		);
	});

	it('should declare metadata for non-static class properties', function() {
		class A { @Inject('Token') public value: any; }

		expect(
			Reflect.getMetadata(MetadataSymbol.INJECTIONS, A),
			'to contain',
			{ token: 'Token', propertyKey: 'value', injectType: InjectType.PROPERTY }
		);
	});

	it('should throw an error if a static class property is decorated', function() {
		expect(
			() => {
				class A { @Inject('Token') public static value: any; }

				return A;
			},
			'to throw',
			'Cannot use the Inject decorator on static class properties'
		);
	});

	it('should throw an error if a non-constructor method parameter is decorated', function() {
		expect(
			() => {
				class A { public someMethod(@Inject('Token') value: any) { return value; } }

				return A;
			},
			'to throw',
			'Cannot use the Inject decorator on method parameters outside of the constructor'
		);
	});

	it('should throw an error if a static non-constructor method parameter is decorated', function() {
		expect(
			() => {
				class A { public static someMethod(@Inject('Token') value: any) { return value; } }

				return A;
			},
			'to throw',
			'Cannot use the Inject decorator on method parameters outside of the constructor'
		);
	});
});
