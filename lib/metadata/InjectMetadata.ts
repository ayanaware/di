import { ForwardRef } from '../forwardRef';
import { InjectType, Token } from '../types';

/**
 * Metadata for an injection.
 */
export interface InjectMetadata {
	/**
	 * The token of the injection.
	 * If this is not set then the {@link Injector} will try to check the type metadata added by the TypeScript compiler.
	 */
	token?: Token | ForwardRef<Token>;
	/**
	 * The way in which this injection was done.
	 * The {@link Inject} decorator itself will only use {@link InjectType.PROPERTY} and {@link InjectType.CONSTRUCTOR}.
	 */
	injectType: InjectType;
	/**
	 * The property key of the injection.
	 * This is only set when {@link InjectMetadata.injectType} is set to {@link InjectType.PROPERTY}.
	 */
	propertyKey?: string | symbol;
	/**
	 * The parameter index of the injection.
	 * This is only set when {@link InjectMetadata.injectType} is set to {@link InjectType.CONSTRUCTOR}.
	 */
	parameterIndex?: number;
}
