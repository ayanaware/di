/**
 * Metadata for an injectable.
 */
export interface InjectableMetadata {
	/**
	 * Declares an injectable as multi.
	 *
	 * @see {@link Record.multi}
	 */
	multi?: boolean;
	/**
	 * Declares that an injectable should not be instantiated right away but only when it's needed.
	 * Setting this to *true* for a multi-injectable will have no effect, as all multi-injectables are lazily instantiated.
	 *
	 * @see {@link Record.lazy}
	 */
	lazy?: boolean;
}
