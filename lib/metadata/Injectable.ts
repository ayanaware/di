import { InjectableMetadata } from './InjectableMetadata';
import { MetadataSymbol } from './MetadataSymbol';

/**
 * Decorator for an injectable.
 * This decorator is optional but can be used to attach additional metadata.
 *
 * @param meta The metadata to attach to the decorated class
 *
 * @returns *ClassDecorator*
 */
export function Injectable(meta?: InjectableMetadata): ClassDecorator {
	return function(target: any) {
		const metadata: InjectableMetadata = {
			multi: false,
			lazy: false,
			...meta,
		};

		metadata.multi = Boolean(metadata.multi);
		metadata.lazy = Boolean(metadata.lazy);

		Reflect.defineMetadata(MetadataSymbol.INJECTABLE, metadata, target);
	};
}
