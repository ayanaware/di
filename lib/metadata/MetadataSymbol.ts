/**
 * Symbol storage for metadata keys.
 */
export class MetadataSymbol {
	/**
	 * Metadata key for storing an array of {@link InjectMetadata}
	 */
	public static INJECTIONS = Symbol('injections');
	/**
	 * Metadata key for storing an array of {@link InjectableMetadata}
	 */
	public static INJECTABLE = Symbol('injectable');
}
