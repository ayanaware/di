import '@ayana/test';

import 'reflect-metadata';

import { forwardRef } from './forwardRef';
import { Injector } from './Injector';
import { Inject, Injectable } from './metadata';

autoDescribe(() => {
	it('should allow a circular dependency between 3 injectables', function() {
		class A { @Inject(forwardRef(() => C)) public c: C; }
		class B { @Inject(forwardRef(() => A)) public a: A; }
		class C { @Inject(forwardRef(() => B)) public b: B; }

		const injector = new Injector();
		injector.resolveAndCreate(A);
		injector.verify();

		expect(injector.get(A).c, 'to be an instance of', C);
		expect(injector.get(B).a, 'to be an instance of', A);
		expect(injector.get(C).b, 'to be an instance of', B);
	});

	it('should throw an error if a constructor injection happens between 3 circular dependent components', function() {
		class A { @Inject(forwardRef(() => C)) public c: C; }
		class B { public constructor(@Inject(forwardRef(() => A)) public a: A) {} }
		class C { @Inject(forwardRef(() => B)) public b: B; }

		const injector = new Injector();
		expect(
			() => injector.resolveAndCreate(A),
			'to throw',
			'Circular constructor dependency detected: C <=P= B <=C= A <=P= C'
		);
	});

	it('should allow an injetable to inject itself', function() {
		class A { @Inject(A) public a: A; }

		const injector = new Injector();
		injector.resolveAndCreate(A);
		injector.verify();

		expect(injector.get(A).a, 'to be', injector.get(A));
	});

	it('should throw an error when a circular constructor dependency to the same injectable is found', function() {
		class A { public constructor(@Inject(A) public a: A) {} }

		const injector = new Injector();
		expect(
			() => injector.resolveAndCreate(A),
			'to throw',
			'Circular constructor dependency detected: A <=C= A'
		);
	});

	it('should allow the injection of values', function() {
		class A { public constructor(@Inject('Value') public value: string) {} }

		const injector = new Injector();
		injector.provide({ provide: 'Value', useValue: 'SomeValue' });
		injector.resolveAndCreate(A);
		injector.verify();

		expect(injector.get('Value'), 'to be', 'SomeValue');
		expect(injector.get(A), 'to be an instance of', A);
		expect(injector.get(A).value, 'to be', 'SomeValue');
	});

	it('should use an existing dependency with resolveAndCreate', function() {
		class A { }
		class B { public constructor(@Inject(A) public a: A) {} }

		const aInstance = new A();

		const injector = new Injector();

		injector.provide({ provide: A, useInstance: aInstance });
		injector.resolveAndCreate(B);
		injector.verify();

		expect(injector.get(A), 'to be', aInstance);
		expect(injector.get(B).a, 'to be', aInstance);
	});

	it('should resolve dependencies for a type that has already been provided but passed to resolveAndCreate', function() {
		class A { }
		class B { @Inject(A) public a: A; }

		const bInstance = new B();

		const injector = new Injector();

		injector.provide({ provide: B, useInstance: bInstance });
		injector.resolveAndCreate(B);
		injector.verify();

		expect(injector.get(A), 'to be an instance of', A);
		expect(injector.get(B).a, 'to be', injector.get(A));
	});

	it('should throw an error if a null provider is passed', function() {
		const injector = new Injector();

		expect(
			() => injector.provide(null),
			'to throw',
			'Invalid provider: null'
		);
	});

	it('should throw an error if a non-object or non-function provider is passed', function() {
		const injector = new Injector();

		expect(
			() => injector.provide(undefined),
			'to throw',
			'Invalid provider type: undefined'
		);

		expect(
			() => injector.provide('' as any),
			'to throw',
			'Invalid provider type: string'
		);
	});

	it('should throw an error if an invalid provider is passed', function() {
		const injector = new Injector();

		expect(
			() => injector.provide({} as any),
			'to throw',
			'Invalid provider object'
		);
	});

	it('should handle an InstanceProvider properly', function() {
		class Injection { }
		class ProvidedAsInstance { @Inject(Injection) public someInjection: Injection; public constructor(@Inject() public neverHappening: any) {} }

		const instance = new ProvidedAsInstance('nah');

		const injector = new Injector();
		injector.provide(Injection);

		injector.provide({ provide: ProvidedAsInstance, useInstance: instance });
		injector.verify();

		expect(injector.get(ProvidedAsInstance), 'to be', instance);
		expect(injector.get(ProvidedAsInstance).someInjection, 'to be an instance of', Injection);
		expect(injector.get(ProvidedAsInstance).neverHappening, 'to be', 'nah');
	});

	it('should throw an error if the instance of an InstanceProvider does not have a constructor', function() {
		class ProvidedAsInstance { }

		const injector = new Injector();

		expect(
			() => injector.provide({ provide: ProvidedAsInstance, useInstance: null }),
			'to throw',
			'InstanceProvider does not have a constructor. Token: ProvidedAsInstance'
		);
	});

	it('should throw an error if the class of an InstanceProvider is declared as multi', function() {
		@Injectable({ multi: true }) class ProvidedAsInstance { }

		const injector = new Injector();

		expect(
			() => injector.provide({ provide: ProvidedAsInstance, useInstance: new ProvidedAsInstance() }),
			'to throw',
			'InstanceProvider type must not have multi declared. Token: ProvidedAsInstance'
		);
	});

	it('should handle an ExistingProvider properly', function() {
		const injector = new Injector();

		injector.provide({ provide: 'SubToken', useValue: 'Value' });
		injector.provide({ provide: 'Token', useExisting: 'SubToken' });
		injector.verify();

		expect(injector.get('SubToken'), 'to be', injector.get('Token'));
	});

	it('should handle a circular dependency using an ExistingProvider', function() {
		class A { @Inject(forwardRef(() => B)) public b: B; }
		class B { @Inject('ExistingProviderForA') public a: A; }

		const injector = new Injector();

		injector.provide({ provide: 'ExistingProviderForA', useExisting: A });
		injector.resolveAndCreate(A);
		injector.verify();
	});

	it('should throw an error if an ExistingProvider references itself', function() {
		const injector = new Injector();

		expect(
			() => injector.provide({ provide: 'A', useExisting: 'A' }),
			'to throw',
			'Circular existing dependency detected: A <=E= A'
		);
	});

	it('should throw an error if only ExistingProviders circulary reference each other', function() {
		const injector = new Injector();

		injector.provide({ provide: 'A', useExisting: 'B' });

		expect(
			() => injector.provide({ provide: 'B', useExisting: 'A' }),
			'to throw',
			'Circular existing dependency detected: A <=E= B <=E= A'
		);
	});

	it('should return null if an ExistingProvider references a non-existent provider', function() {
		const injector = new Injector();

		injector.provide({ provide: 'A', useExisting: 'B' });

		expect(injector.get('A'), 'to be null');
	});

	it('should throw an error if multi providers circularly reference each other', function() {
		@Injectable({ multi: true }) class A { @Inject(forwardRef(() => B)) public b: B; }
		@Injectable({ multi: true }) class B { @Inject(forwardRef(() => A)) public a: A; }

		const injector = new Injector();
		injector.provide(A);

		expect(
			() => injector.provide(B),
			'to throw',
			'Circular multi dependency detected: A(M) <=P= B(M) <=P= A(M)'
		);
	});

	it('should handle a multi injectable properly', function() {
		let failInstantiation = true;

		@Injectable({ multi: true }) class A { public constructor() { if (failInstantiation) throw new Error('Instantiation not allowed yet'); }}

		const injector = new Injector();
		injector.provide(A);

		failInstantiation = false;

		const instance1 = injector.get(A);
		const instance2 = injector.get(A);

		expect(instance1, 'to be an instance of', A);
		expect(instance2, 'to be an instance of', A);
		expect(instance1, 'not to be', instance2);
	});

	it('should handle a lazy injectable properly', function() {
		let failInstantiation = true;

		@Injectable({ lazy: true }) class A { public constructor() { if (failInstantiation) throw new Error('Instantiation not allowed yet'); }}

		const injector = new Injector();
		injector.provide(A);

		failInstantiation = false;

		const instance1 = injector.get(A);
		const instance2 = injector.get(A);

		expect(instance1, 'to be an instance of', A);
		expect(instance2, 'to be an instance of', A);
		expect(instance1, 'to be', instance2);
	});
});
