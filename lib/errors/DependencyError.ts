import { GenericError } from '@ayana/errors';

/**
 * Error used when something goes wrong while adding, resolving or linking dependencies.
 */
export class DependencyError extends GenericError {}
