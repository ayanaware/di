import { IllegalAccessError, IllegalArgumentError, IllegalStateError } from '@ayana/errors';

import { DependencyError, InjectionError, InstantiationError } from './errors';
import { resolveForwardRef } from './forwardRef';
import { InjectableMetadata, InjectMetadata, MetadataSymbol } from './metadata';
import { ClassProvider, DependencyRecord, Dependent, ExistingProvider, FactoryProvider, InjectType, isType, Provider, Record, Token, Type } from './types';

/**
 * @ignore
 */
const EMPTY = {};

/**
 * @ignore
 *
 * @param token The token to check
 *
 * @returns *true* if the token is an Injector type, *false* if otherwise
 */
function isInjectorType<T extends Injector>(token: any): token is Type<T> {
	return token && token.prototype instanceof Injector || token === Injector;
}

/**
 * The options for an {@link Injector}.
 */
export interface InjectorOptions {
	/**
	 * Whether to support circular dependencies or not.
	 */
	allowCircularDependencies?: boolean;
	/**
	 * Whether to allow dependencies to inject the current injector or not.
	 */
	allowInjectorInjection?: boolean;
}

/**
 * Does all the Dependency Injection.
 */
export class Injector {
	private readonly parentInjector: Injector;

	private readonly pending: Map<Token, Record> = new Map();
	private readonly instantiated: Map<Token, Record> = new Map();
	private readonly records: Map<Token, Record> = new Map();

	private readonly options: InjectorOptions;

	/**
	 * Boolean to be used to prevent calls to {@link Injector.handlePendingRecords} to do something.
	 */
	protected runHandlePendingRecords: boolean = true;

	/**
	 * Creates a new Injector.
	 *
	 * @param options The options for this injector
	 * @param parentInjector An optional parent injector. It is recommmended to use the {@link Injector.createChildInjector} method instead
	 */
	public constructor(options?: InjectorOptions, parentInjector?: Injector) {
		this.options = Object.freeze({ ...{
			allowCircularDependencies: true,
			allowInjectorInjection: true,
		} as InjectorOptions, ...options });

		this.parentInjector = parentInjector instanceof Injector ? parentInjector : null;
	}

	/**
	 * Returns this injector.
	 *
	 * @param token A token that indicates to return this Injector
	 *
	 * @returns This injector
	 *
	 * @throws {@link IllegalArgumentError} When this Injector is not an instance of the given Injector token
	 * @throws {@link IllegalAccessError} When this Injector has disabled the {@link InjectorOptions.allowInjectorInjection} option
	 */
	public get<T extends Injector>(token: Type<T>): T;
	/**
	 * Returns a tokens value by first checking records in this Injector and then checking the ones in the parent Injector if it exists.
	 * If no record could be found then *null* is returned.
	 * In theory a record could have been found where the value is set to *null*.
	 * To detect that use the {@link Injector.has} method.
	 *
	 * @param token The token to be checked
	 *
	 * @returns The tokens value or *null* if no record could be found
	 *
	 * @throws {@link InstantiationError} When the instantiation of a lazy-record fails
	 * @throws {@link DependencyError} When there is an issue while injecting dependencies for a lazy-record
	 */
	public get<T>(token: Type<T> | Exclude<Token, Type<any>>): T;
	public get<T, I extends Injector>(token: Type<T> | Exclude<Token, Type<any>> | Type<I>): T | I {
		// Check records for token
		if (this.records.has(token)) {
			return this.getFromMap('records', token);
		}

		// Check for injector token
		if (isInjectorType(token)) {
			// Check whether injector injection is allowed
			if (!this.options.allowInjectorInjection) throw new IllegalAccessError(`The Injector has disabled that it can be injected`);
			// Make sure this injector is an instance of the requested token
			if (!(this instanceof token)) throw new IllegalArgumentError(`This injector is not an instance of ${token.name}`);

			return this;
		}

		// Forward to parent injector if it exists
		if (this.hasParent()) {
			return this.parentInjector.get(token);
		}

		return null;
	}

	/**
	 * Returns whether a Token can be provided by this Injector or a parent of this Injector.
	 *
	 * @param token The token to check
	 *
	 * @returns true if the token can be provided, false if otherwise
	 */
	public has(token: Token): boolean {
		// Check records in this injector
		if (this.records.has(token)) return true;

		// Check for injector injection
		if (isInjectorType(token)) {
			// Return false if the user tries to ask for the Injector but this one isn't injectable
			return this.options.allowInjectorInjection && this instanceof token;
		}

		// Check the parent injector if it exists
		return this.hasParent() && this.parentInjector.has(token);
	}

	/**
	 * Returns whether a token is known in this Injector or a parent Injector.
	 * The difference between {@link Injector.has} is that the pending and instantiated maps of each Injector are checked too.
	 *
	 * @param token The token to check
	 *
	 * @returns true if the token is known, false if otherwise
	 */
	public knownToken(token: Token): boolean {
		// Check records in this injector
		if (this.records.has(token) || this.pending.has(token) || this.instantiated.has(token)) return true;

		// Check for injector injection
		if (isInjectorType(token)) {
			// Return false if the user tries to ask for the Injector but this one isn't injectable
			return this.options.allowInjectorInjection && this instanceof token;
		}

		// Check the parent injector if it exists
		return this.hasParent() && this.parentInjector.knownToken(token);
	}

	/**
	 * Returns whether this Injector has a parent or not.
	 *
	 * @returns true if this Injector has a parent, false if otherwise
	 */
	public hasParent(): boolean {
		return this.parentInjector !== null;
	}

	/**
	 * Adds a {@link Provider} to this {@link Injector}.
	 *
	 * @param provider The {@link Provider} to be added
	 *
	 * @throws {@link DependencyError} When the {@link Provider} is invalid
	 * @throws {@link DependencyError} When {@link InstanceProvider.useInstance} does not have a constructor
	 * @throws {@link DependencyError} When {@link InstanceProvider.useInstance}s class has {@link Injectable.multi} or {@link Injectable.lazy} declared
	 * @throws {@link DependencyError} When the {@link Token} is already known by this {@link Injector} or a parent {@link Injector}
	 *
	 * @see {@link Provider}, {@link ValueProvider}, {@link ClassProvider}, {@link InstanceProvider}, {@link FactoryProvider}, {@link ExistingProvider}, {@link TypeProvider}
	 */
	public provide(provider: Provider) {
		if (provider === null) throw new DependencyError(`Invalid provider: null`);
		if (typeof provider !== 'function' && typeof provider !== 'object') {
			throw new DependencyError(`Invalid provider type: ${typeof provider}`);
		}

		let token: Token;
		const record: Record = {
			deps: [],
			fn: null,
			useNew: false,
			value: EMPTY,
			multi: false,
			lazy: false,
			existing: false,
		};

		if ('useValue' in provider) {
			token = provider.provide;
			record.value = provider.useValue;
		} else if ((provider as ClassProvider).useClass || isType(provider)) {
			let type: Type<any>;
			if (isType(provider)) {
				token = provider;
				type = provider;
			} else {
				token = (provider as ClassProvider).provide;
				type = resolveForwardRef((provider as ClassProvider).useClass);
			}

			const meta = this.resolveInjectableMeta(type);

			record.fn = type;
			record.useNew = true;
			record.deps = this.resolveDependencies(type);
			record.multi = meta.multi;
			// A multi-injectable is always lazy
			record.lazy = record.multi || meta.lazy;
		} else if ('useInstance' in provider) {
			token = provider.provide;
			record.value = provider.useInstance;

			const type = record.value != null ? record.value.constructor : null;
			if (typeof type !== 'function') {
				throw new DependencyError(`InstanceProvider does not have a constructor. Token: ${this.stringifyToken(token)}`);
			}

			const meta = this.resolveInjectableMeta(type);
			record.deps = this.resolveDependencies(type);

			// If multi or lazy is set on the class we throw an error because those are not supported for instance providers
			if (meta.multi || meta.lazy) {
				throw new DependencyError(`InstanceProvider type must not have ${meta.multi ? 'multi' : 'lazy'} declared. Token: ${this.stringifyToken(token)}`);
			}

			// Remove constructor injections because we can't handle them
			record.deps = record.deps.filter(dep => typeof dep.parameterIndex !== 'number');
		} else if ((provider as FactoryProvider).useFactory) {
			token = (provider as FactoryProvider).provide;

			record.fn = (provider as FactoryProvider).useFactory;
			// The dependencies will be injected after the order of the given deps array
			record.deps = (provider as FactoryProvider).deps.map((v, i) => {
				return {
					token: v,
					injectType: InjectType.CONSTRUCTOR,
					parafnmeterIndex: i,
				} as DependencyRecord;
			});
			record.multi = Boolean((provider as FactoryProvider).multi);
			// A multi-injectable is always lazy
			record.lazy = record.multi || Boolean((provider as FactoryProvider).lazy);
		} else if ((provider as ExistingProvider).useExisting) {
			token = (provider as ExistingProvider).provide;

			// Declare the dependency so this doesn't get moved to records too early
			// We also need this for the circular dependency detection
			record.deps = [{
				token: (provider as ExistingProvider).useExisting,
				injectType: InjectType.UNKNOWN,
			}];

			record.existing = true;
			record.value = (provider as ExistingProvider).useExisting;
		} else {
			throw new DependencyError('Invalid provider object');
		}

		if (this.knownToken(token)) throw new DependencyError('Duplicate token: ' + this.stringifyToken(token));

		this.pending.set(token, record);

		this.handlePendingRecords();
	}

	/**
	 * Verifies this Injector and throws an error if one or more records are pending.
	 * This method does **NOT** verify the parent Injector.
	 *
	 * @throws {@link IllegalStateError} When one or more records are pending including detailed information about the dependency map
	 */
	public verify(): void {
		const missingForInstantiation = this.getMissingDependencies('instantiating');
		const missingForLinking = this.getMissingDependencies('linking');

		if (missingForInstantiation.size < 1 && missingForLinking.size < 1) return;

		let message = `One or more records are still in a pending state:\n`;
		if (missingForInstantiation.size > 0) {
			message += `    Missing for instantiation:\n`;
			for (const entry of missingForInstantiation.entries()) {
				message += `        ${this.stringifyToken(entry[0])} is missing: ${entry[1].map(t => this.stringifyToken(t)).join(', ')}\n`;
			}
		}
		if (missingForLinking.size > 0) {
			message += `    Missing for linking:\n`;
			for (const entry of missingForLinking.entries()) {
				message += `\n        ${this.stringifyToken(entry[0])} is missing: ${entry[1].map(t => this.stringifyToken(t)).join(', ')}\n`;
			}
		}

		throw new IllegalStateError(message);
	}

	/**
	 * Creates a new child Injector.
	 *
	 * @param options The options for this injector
	 * @returns A new Injector instance
	 */
	public createChildInjector(options?: InjectorOptions): Injector {
		return new Injector(options, this);
	}

	/**
	 * Resolves and creates dependencies starting from one {@link Type} and recursively checking other {@link Type} references and also adding them.
	 * If there are any non-{@link Type} dependencies those will be skipped but this method will not throw an error on missing dependencies either.
	 * If a {@link Type} has already been provided beforehand this method will not create a new {@link Record} for it but still resolve it's dependencies.
	 * Use the {@link Injector.verify} method to check whether everything has been resolved or not.
	 *
	 * @param type The {@link Type} (class) to start resolving at
	 */
	public resolveAndCreate(type: Type<any>): void {
		this.runHandlePendingRecords = false;

		try {
			this.resolveAndCreateRecursively(type);
		} finally {
			this.runHandlePendingRecords = true;
		}

		this.handlePendingRecords();
	}

	protected resolveAdditionalDependencies(record: Record): Array<Token> {
		return [];
	}

	protected onRecordsLinked(records: Array<Record>): void {
		//
	}

	protected onRecordInstantiated(record: Record, multiInstance?: any): void {
		//
	}

	protected stringifyToken(token: Token): string {
		if (isType(token)) return token.name;

		return String(token);
	}

	protected getRecord(token: Token, checkParent: boolean = true): Record {
		// Check records in this injector
		const record = this.records.get(token) || this.pending.get(token) || this.instantiated.get(token);

		if (record != null) return record;

		// Check the parent injector if it exists
		return checkParent && this.hasParent() ? this.parentInjector.getRecord(token) : null;
	}

	protected handlePendingRecords() {
		if (!this.runHandlePendingRecords) return;

		this.runHandlePendingRecords = false;

		try {
			let loaded = 0;

			const missing = this.getMissingDependencies('instantiating');

			for (const entry of this.pending.entries()) {
				const token = entry[0];
				if (missing.has(token)) continue;

				const record = entry[1];
				if (record.lazy) {
					// If the record is lazy we only instantiate on demand
					// We made sure all dependencies for instantiating are given so we can just move it over
					this.pending.delete(token);
					this.instantiated.set(token, record);
				} else {
					// If the record is a single instance we can instantiate it directly
					this.instantiateRecord(token, record);
				}

				loaded++;
			}

			if (loaded > 0) {
				this.linkRecords();

				this.runHandlePendingRecords = true;

				this.handlePendingRecords();
			}
		} finally {
			this.runHandlePendingRecords = true;
		}
	}

	private resolveAndCreateRecursively(type: Type<any>, checkedTypes: Array<Type<any>> = []) {
		// Add to checked types
		checkedTypes.push(type);

		// Provide if token is still unknown
		if (!this.knownToken(type)) {
			this.provide(type);
		}

		// Record should exist by now
		const record = this.getRecord(type, false);

		for (const dep of record.deps) {
			// TODO: Injector type needs test
			// If the dependency is not a type or we have already checked it or it is an injector type we can continue
			if (!isType(dep.token) || checkedTypes.includes(dep.token) || isInjectorType(dep.token)) continue;

			this.resolveAndCreateRecursively(dep.token, checkedTypes);
		}
	}

	private getFromMap(map: 'records' | 'instantiated', token: Token): any {
		const useMap = map === 'instantiated' ? this.instantiated : this.records;
		const record = useMap.get(token);

		if (record.existing) {
			// The records value represents the token of the actual provider
			return this.getFromMap(map, record.value);
		}
		if (record.lazy) {
			if (record.value !== EMPTY) {
				// Return the records value if it's not empty
				return record.value;
			}

			// Every lazy record was approved for instantiation by handlePendingRecords so instantiating is safe
			const instantiated = this.instantiateRecord(token, record);

			// getFromMap with instantiated will only be called by linkRecords which also made sure this record has all linking dependencies
			// if it's in the record map already it was also approved already
			this.linkRecord(record, instantiated);

			return instantiated;
		}

		// If it's not a lazy record we can just use value
		return record.value;
	}

	private instantiateRecord(token: Token, record: Record): any {
		if (record.value === EMPTY) {
			const fnDeps: Array<any> = [];
			if (isType(record.fn)) {
				for (const dep of record.deps) {
					if (typeof dep.parameterIndex !== 'number') continue;

					// Token null check has been performed by getMissingDependencies in handlePendingRecords
					// If this is called by getFromMap then it already went through said method above

					if (!this.has(dep.token)) {
						throw new InjectionError(`Constructor-Injection for ${record.fn.name} failed: Could not find record for token: ${this.stringifyToken(dep.token)}`);
					}

					const injectable = this.get(dep.token);

					fnDeps[dep.parameterIndex] = injectable;
				}
			}

			try {
				record.value = record.useNew ? new (record.fn as any)(...fnDeps) : record.fn(...fnDeps);
			} catch (e) {
				throw new InstantiationError(`Failed to instantiate record with token: ${this.stringifyToken(token)}`).setCause(e);
			}
		}

		const instance: any = record.value;
		if (record.multi) {
			record.value = EMPTY;
		} else if (!record.lazy) {
			// Additional dependencies can only work for non-lazy records
			// Otherwise this would invalidate the missing dependencies array after the record is instantiated on demand
			const mappedToToken = record.deps.map(depRecord => depRecord.token);
			this.resolveAdditionalDependencies(record).forEach(depToken => {
				if (!mappedToToken.includes(depToken)) {
					record.deps.push({
						token: depToken,
						injectType: InjectType.UNKNOWN,
					});
				}
			});

			// Map moving is also only done for non-lazy records
			// It's done somewhere else for lazy records
			this.pending.delete(token);
			this.instantiated.set(token, record);
		}

		this.onRecordInstantiated(record, instance);

		return instance;
	}

	private detectCircularDependency(startToken: Token, dependents: Map<Token, Array<Dependent>>) {
		this._detectCircularDependency({ token: startToken, injectedThrough: null, multi: null, existing: null }, dependents);
	}

	private _detectCircularDependency(current: Dependent, dependents: Map<Token, Array<Dependent>>, path?: Array<Dependent>) {
		const dependentsOfToken = dependents.get(current.token);
		// If nothing is dependent on the current token anymore we can stop
		if (dependentsOfToken == null) return;

		// The first call should not push the dependent because it's not a dependent object inside the dependents map
		if (path == null) {
			path = [];
		} else {
			path = [...path];
			path.push(current);
		}

		for (const dependentOfToken of dependentsOfToken) {
			if (path.includes(dependentOfToken)) {
				// We found a circular dependency
				path.push(dependentOfToken);

				// Reverse the path so we have the actual dependency order
				path.reverse();
				// Remove all elements after we find the current one to remove everything leading into this circular dependency
				// For example if the path would be BCABD because D lead into the cicular dependency with ABC but isn't part of it it should be removed
				path.splice(0, path.findIndex(v => v === dependentOfToken));
				// We ignore the last element in the check because it could have been injected by something
				// not part of the circular dependency which is perfectly valid, but we care about the injection method
				// In addition we know that the first and last element have the same token
				path.splice(-1);

				// Check if we have any dependencies injected through the constructor
				const circularWithConstructor = path.some(dependent => dependent.injectedThrough === InjectType.CONSTRUCTOR);

				// If every dependency inside the circular dependency path is an existing dependency we will end up in an infinite loop
				// of the Injector trying to get a value so we have to prevent this
				const circularExisting = path.every(dependent => dependent.existing);

				// If every dependency inside the circular dependency path is a multi-injectable we will end up with an infinite
				// object creation so we have to prevent this
				const circularMultiDependencies = path.every(dependent => dependent.multi);

				// We only have to throw an error if...
				//  - It's circular constructor dependecy
				//  - It's a circular dependency only consisting of multi records
				//  - It's a circular dependency only consisting of existing-ref records
				//  - Circular dependencies are not allowed
				if (!circularMultiDependencies && !circularWithConstructor && !circularExisting && this.options.allowCircularDependencies) return;

				// Stringify logic to show the way the injection was done
				let stringifiedPath = '';
				for (const item of path) {
					stringifiedPath += `${this.stringifyToken(item.token)}${item.multi ? '(M)' : ''}`;

					if (item.injectedThrough === InjectType.CONSTRUCTOR) {
						stringifiedPath += ' <=C= ';
					} else if (item.injectedThrough === InjectType.PROPERTY) {
						stringifiedPath += ' <=P= ';
					} else if (item.existing) {
						stringifiedPath += ' <=E= ';
					} else {
						stringifiedPath += ' <=?= ';
					}
				}

				stringifiedPath += `${this.stringifyToken(path[0].token)}${path[0].multi ? '(M)' : ''}`;

				let dependencyErrorType = 'Circular ';
				if (circularWithConstructor) {
					dependencyErrorType += 'constructor ';
				} else if (circularMultiDependencies) {
					dependencyErrorType += 'multi ';
				} else if (circularExisting) {
					dependencyErrorType += 'existing ';
				}
				dependencyErrorType += 'dependency';

				throw new DependencyError(`${dependencyErrorType} detected: ${stringifiedPath}`);
			}

			this._detectCircularDependency(dependentOfToken, dependents, path);
		}
	}

	private getMissingDependencies(mode: 'instantiating' | 'linking') {
		const dependents: Map<Token, Array<Dependent>> = new Map();
		const missing: Map<Token, Array<Token>> = new Map();

		let recordsToCheck: Map<Token, Record>;

		if (mode === 'instantiating') {
			recordsToCheck = this.pending;
		} else if (mode === 'linking') {
			recordsToCheck = this.instantiated;
		}

		for (const entry of recordsToCheck.entries()) {
			const token = entry[0];
			const multi = entry[1].multi;
			const existing = entry[1].existing;
			const dependencies = entry[1].deps;

			for (const dep of dependencies) {
				if (dep.token == null) {
					throw new DependencyError(`Null-Dependency inside ${this.stringifyToken(token)}! This might be a circular import. (Injection Details => Property Key: ${dep.propertyKey != null ? dep.propertyKey as string : 'N/A'}, Param Index: ${typeof dep.parameterIndex === 'number' ? dep.parameterIndex : 'N/A'})`);
				}

				// If dependency is already done we don't need to care
				if (this.has(dep.token)) continue;

				// Create dependents array if not existing already
				if (!dependents.has(dep.token)) {
					dependents.set(dep.token, []);
				}

				// Add the current token as a dependant
				dependents.get(dep.token).push({
					token,
					multi,
					existing,
					injectedThrough: dep.injectType,
				});

				// We always need to run the circular dependency check because multi and existing dependencies often only get found in the linking stage
				this.detectCircularDependency(token, dependents);

				// Only dependencies that are not in the records map get up to here
				// A dependency is missing if...
				// - The mode is linking and it cannot be found in the instantiated array
				// - It is injected via constructor
				if ((mode === 'linking' && !this.instantiated.has(dep.token)) || dep.injectType === InjectType.CONSTRUCTOR) {
					// Create missing array if not existing already
					if (!missing.has(token)) {
						missing.set(token, []);
					}

					// Push the dependency token into the missing array of the current token
					missing.get(token).push(dep.token);
				}
			}
		}

		// Mark all records as missing that depend on a missing record
		for (const token of missing.keys()) {
			this.markDependentsAsMissing(mode, token, dependents, missing);
		}

		return missing;
	}

	private markDependentsAsMissing(mode: 'linking' | 'instantiating', token: Token, dependents: Map<Token, Array<Dependent>>, missing: Map<Token, Array<Token>>) {
		if (dependents.has(token)) {
			for (const dependent of dependents.get(token)) {
				// Do not mark dependents that don't inject the current token via constructor as missing
				// TODO: Needs test
				if (mode === 'instantiating' && dependent.injectedThrough !== InjectType.CONSTRUCTOR) {
					continue;
				}

				const arrMissing = missing.get(dependent.token) || [];

				if (!arrMissing.includes(token)) arrMissing.push(token);

				const notThere = !missing.has(dependent.token);
				missing.set(dependent.token, arrMissing);

				// Only continue to mark dependents if the current dependent hasn't been added yet
				// If it already has been added that means this function was already for the dependent
				if (notThere) this.markDependentsAsMissing(mode, dependent.token, dependents, missing);
			}
		}
	}

	private linkRecord(record: Record, instance: any) {
		for (const dep of record.deps) {
			// Here we only inject property dependencies
			if (dep.propertyKey == null) continue;

			// Token null check has been performed by getMissingDependencies

			if (!this.has(dep.token) && !this.instantiated.has(dep.token)) {
				throw new InjectionError(`Property-Injection for ${record.fn.constructor.name}.${dep.propertyKey.toString()} failed: Could not find record for token: ${this.stringifyToken(dep.token)}`);
			}

			// Get injection from this injectors records or any of the parents, or from the instantiated map of this injector
			const injection = this.get(dep.token) || this.getFromMap('instantiated', dep.token);

			instance[dep.propertyKey] = injection;
		}
	}

	private linkRecords() {
		const missing = this.getMissingDependencies('linking');

		const loaded: Array<Record> = [];
		for (const entry of this.instantiated.entries()) {
			const token = entry[0];
			if (missing.has(token)) continue;

			const record = entry[1];

			if (!record.lazy) {
				// Only link the record if it's not lazy
				// The linking for lazy ones will be done by getFromMap
				this.linkRecord(record, record.value);
			}

			// Lazy records will also be moved to the record map so we don't have to worry about it as a potentially missing dependency anymore
			this.instantiated.delete(token);
			this.records.set(token, record);

			loaded.push(record);
		}

		this.onRecordsLinked(loaded);
	}

	private resolveDependencies(type: Type<any>): Array<DependencyRecord> {
		const results: Array<DependencyRecord> = [];

		const injectMetadata: Array<InjectMetadata> = Reflect.getMetadata(MetadataSymbol.INJECTIONS, type);
		if (injectMetadata != null) {
			for (const injection of injectMetadata) {
				const result: DependencyRecord = {
					token: null,
					injectType: injection.injectType,
					propertyKey: injection.propertyKey,
					parameterIndex: injection.parameterIndex,
				};

				if (injection.token == null) {
					if (typeof injection.parameterIndex !== 'number') {
						// Fallback to design:type metatada
						result.token = Reflect.getMetadata('design:type', type, injection.propertyKey);
					}
				} else {
					result.token = resolveForwardRef(injection.token);
				}

				results.push(result);
			}
		}

		const constructorDesignTypes: Array<any> = Reflect.getMetadata('design:paramtypes', type);
		if (constructorDesignTypes != null) {
			for (let i = 0; i < constructorDesignTypes.length; i++) {
				let result = results.find(r => r.parameterIndex === i);

				if (result == null) {
					result = {
						token: constructorDesignTypes[i],
						injectType: InjectType.CONSTRUCTOR,
						parameterIndex: i,
					};
					results.push(result);
				} else {
					if (result.token == null) {
						result.token = constructorDesignTypes[i];
					}
				}
			}
		}

		return results;
	}

	private resolveInjectableMeta(type: Type<any>): InjectableMetadata {
		return Reflect.getMetadata(MetadataSymbol.INJECTABLE, type) || {
			multi: false,
			lazy: false,
		};
	}
}
