@ayana/di [![NPM](https://img.shields.io/npm/v/@ayana/di.svg)](https://www.npmjs.com/package/@ayana/di) [![Discord](https://discordapp.com/api/guilds/508903834853310474/embed.png)](https://discord.gg/eaa5pYf) [![Install size](https://packagephobia.now.sh/badge?p=@ayana/di)](https://packagephobia.now.sh/result?p=@ayana/di)
===

[Documentation](https://docs.ayana.io/modules/di.html)

Dependency Injection for Bento, libraries and more

What this is
---

In a nutshell, this package provides you with a fancy way of linking objects together called "Dependency Injection" (DI). The goal of this package is to make DI portable so it can be used inside libraries too and not only applications.
This library was heavily inspired by [Angular's](https://github.com/angular/angular) way of doing DI so some terms may sound familiar.

Installation
---

With NPM

```
npm i @ayana/di
```

With Yarn

```
yarn add @ayana/di
```

Usage
---

Soon™ (Meanwhile, there are some examples inside the Injector.spec.ts file and inside the @ayana/test README)

Reading "Circular dependency" errors
---

Those errors might seem a bit cryptic at first, but they are actually pretty simple to understand.

The basic message structure looks somewhat like this:

```
Circular dependency detected: Injectable1 <=C= Injectable2 <=P= Injectable3(M) <=?= Injectable1
```

The names between the arrows represent a stringified version of the Token that you and `@ayana/di` use as a key to an injectable.
The first and last dependency in such a chain are always the same because we're dealing with a circular dependency chain here.
If there's an `(M)` after the stringified Token, that indicates that the dependency is a multi-injectable.

The arrows point form right to left so you can read each arrow as `injects` i.e. `Injectable1 injects Injectable2`, `Injectable2 injects Injectable3` and so on.
If you would read it from right to left it would become `is injected into` i.e. `Injectable2 is injected into Injectable1` and so on.

The letter in the arrow represents the way the injection was done. There are currently 4 characters that can appear here:

|Letter|Explaination|
|------|------------|
|C|The dependency was injected via constructor. This is likely the cause of issues with circular dependencies. Read the troubleshooting section for more information| 
|E|The dependency was referenced by the `ExistingProvider` to the left of the arrow. This is usually not the cause of an issue|
|P|The dependency was injected via property|
|?|It is unknown how the injection was done, but a dependency was declared. This should only appear when using custom injectors|

Let's break some examples down:

```
Circular constructor dependency detected: C <=P= B <=C= A <=P= C
```

The text `Circular constructor dependency` already tells us that we are injecting something circularly with at least one constructor injection.
If we read the error out we get `C injects B, B injects A, A injects C`. We also see a `<=C=` between `B` and `A` which already tells us that one issue is there. When looking at the other arrows we only have `<=P=` which means they are likely not involved in this specific error.

In this case we need to remove the constructor injection inside `B` and replace it with a property injection, or remove it completely.

```
Circular existing dependency detected: A <=E= B <=E= A
```

The text `Circular existing dependency` already tells us that we are circularly referencing `ExistingProvider`s.
When we read the error out we get `A injects B, B injects A`. The `<=E=` tells us that it is a reference by an `ExistingProvider`.

In this case we need to remove either the existing provider `A` or `B` and replace it with a "real" provider.

```
Circular multi dependency detected: A(M) <=P= B(M) <=P= A(M)
```

The text `Circular multi dependency` already tells us that we are circularly referencing providers declared as multi.
When we read the error out we get `A injects B, B injects A`. The `(M)` tells us that it is a multi dependency.

In this case we need to either mark `A` or `B` as non-multi, or remove the dependency from one to the other.

Troubleshooting
---

### I'm getting the error "Circular constructor/multi/existing dependency detected"

This could be due to 2 reasons:

1. The option `allowCircularDependencies` was set to `false`, meaning no circular dependencies at all are allowed, so you have to either enable that option or get rid of the circular dependency
2. You attempted to make a circular dependency in a way that is not supported by `@ayana/di`, like making a circular dependency chain only consisting of multi or existing dependencies, or making a circular dependency chain that consists of at least one constructor injection.

The error is usually pretty informative so you can figure out the problem yourself after checking out the guide about how to read those errors.
If you still need help however, you can join our [Discord Server](https://discord.gg/eaa5pYf).

Testing with [@ayana/test](https://www.npmjs.com/package/@ayana/test)
---

See the [README of the @ayana/test module](https://www.npmjs.com/package/@ayana/test#testing-with-ayanadi) for more information about how to test software that uses @ayana/di.

Links
---

[GitLab repository](https://gitlab.com/ayana/libs/di)

[NPM package](https://npmjs.com/package/@ayana/di)

License
---

Refer to the [LICENSE](LICENSE) file.
